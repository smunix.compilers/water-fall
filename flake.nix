{
  description = "Rain water problem";
  # url : https://codepumpkin.com/trapping-rain-water-algorithm-problem/
  inputs = {
    np.url = "github:nixos/nixpkgs?ref=haskell-updates";
    fu.url = "github:numtide/flake-utils?ref=master";
    nf.url = "github:numtide/nix-filter?ref=master";
  };
  outputs = { self, np, fu, nf }:
    with fu.lib;
    with np.lib;
    with nf.lib;
    eachSystem [ "x86_64-linux" ] (system:
      let
        version =
          "${substring 0 8 self.lastModifiedDate}.${self.shortRev or "dirty"}";
        config = { };
        overlay = final: _:
          with final;
          with (haskellPackages.extend (final: _: { }));
          with haskell.lib; {
            water-fall = (callCabal2nix "water-fall" (filter {
              root = ./.;
              exclude = [ (matchExt "cabal") ];
            }) { }).overrideAttrs (o: { version = "${o.version}-${version}"; });
          };
        overlays = [ overlay ];
      in with (import np { inherit system config overlays; }); rec {
        packages = flattenTree (recurseIntoAttrs { inherit water-fall; });
      });
}
