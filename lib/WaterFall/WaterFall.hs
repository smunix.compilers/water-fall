-- |
module WaterFall.WaterFall where

import Data.Array

{-
https://codepumpkin.com/trapping-rain-water-algorithm-problem/
Case 3 :

Input = [ 1, 4, 2, 5, 0, 6, 2, 3, 4]

Output = 2 + 5 + 2 + 1
               = 10
-}

fromList :: forall a. [a] -> Array Int a
fromList list = array (1, len) (zip [1 ..] list)
  where
    len = length list

-- >>> fromList [ 1, 4, 2, 5, 0, 6, 2, 3, 4]
-- array (1,9) [(1,1),(2,4),(3,2),(4,5),(5,0),(6,6),(7,2),(8,3),(9,4)]

waterVolume :: [Int] -> Int
waterVolume (fromList -> arr@(bounds -> (minB, maxB))) = walk 0 minB maxB
  where
    walk :: Int -> Int -> Int -> Int
    walk acc lpos rpos
      | lpos >= rpos = acc
      | lHeight < rHeight = stepF lHeight 1 acc lpos (\acc' pos' -> walk acc' pos' rpos)
      | otherwise = stepF rHeight (-1) acc rpos (\acc' pos' -> walk acc' lpos pos')
      where
        (lHeight, rHeight) = (arr ! lpos, arr ! rpos)

    stepF :: Int -> Int -> Int -> Int -> (Int -> Int -> Int) -> Int
    stepF maxHeight dir acc pos kont
      | delta <= 0 = kont acc pos'
      | otherwise = stepF maxHeight dir (acc + delta) pos' kont
      where
        delta = maxHeight - arr ! pos'
        pos' = pos + dir

-- >>> waterVolume [ 1, 4, 2, 5, 0, 6, 2, 3, 4 ]
-- 10

-- >>> waterVolume  [0, 4, 5, 1]
-- 0

-- >>> waterVolume [3,0,3]
-- 3
